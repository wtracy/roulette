# roulette

A simulation of a simple Martingale strategy for Roulette.

Example run:

```
~/roulette$ ./roulette.py 
Insolvent 1 out of 1,000 attempts.
Net winnings: $999 Net losses: $-1,023
Average win amount: $1.00
Average loss amount: $-1023.00
Won $-24 overall.
~/roulette$
```

## The System

The Martingale strategy involves doubling your bet every time you lose. The 
rationale is that any one win will pay off all your previous losses.

Here, we define a "session" as a sequence of bets that start at $1, and end
whenever either the player comes out ahead, or the player's accumulated losses
for the session exceeds their credit limit.

A session may be as short as one bet that wins. In any case, after a session
ends, the next session starts again with a bet of $1. (In the case of a losing
session, this presumably happens after the player works off their debt and
returns to the casino.)

Our program simulates a number of sessions defined by the `TRIES` variable.
Upon completing, it prints out the number of sessions that ended in insolvency,
the total (net) results of the winning and losing sessions, the average amount 
won from a winning session, the average amount lost from a losing session, and
the player's total take.

The code simulates bets placed on black or red color, resulting in each bet
having a roughly 47% chance of winning (with American-style rules).

## The Code

The program opens with a series of constants that define the player's loss/debt 
cap, the number of sessions to try, and the details of the roulette game. By
default, it simulates American-style roulette with 36 numbered slots and two
zeroes slots.

In the `runSimulation` function, the `take` variable stores the amount the 
player has won or loss in the current session, `bet` stores the size of the 
next bet to be placed, and `net` stores the cumulative take of all sessions so 
far.

## Test and Test Coverage Results

Live test coverage report is available at https://wtracy.gitlab.io/roulette
