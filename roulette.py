#!/usr/bin/python3

"""A simulation of a simple Martingale strategy for Roulette."""
import random
import locale
import sys
locale.setlocale(locale.LC_ALL, '')

NUM_NUMBERED_SLOTS = 36
NUM_ZEROES = 2
NUM_SLOTS = NUM_NUMBERED_SLOTS + NUM_ZEROES

def isWin(result):
    """Determine if the spin was a win.

    Checks whether the given number is in the first half
    of the numbered slots.
    >>> isWin(0)
    True
    >>> isWin(17)
    True
    >>> isWin(18)
    False
    >>> isWin(36)
    False
    """
    return result < NUM_NUMBERED_SLOTS / 2

def calculateTake(oldTake, win, bet):
    """Return the new take.

    Take is caluclated given the outcome of the previous bet, and the
    amount betted.
    >>> calculateTake(10, True, 1)
    11
    >>> calculateTake(0, True, 4)
    4
    >>> calculateTake(10, False, 8)
    2
    """
    if win:
        return oldTake + bet
    return oldTake - bet

def calculateNewBet(old):
    """Return the amount to bet, given the previous bet.

    Since this function is never called at the start of a session, this is always
    double the previous bet.
    >>> calculateNewBet(1)
    2
    >>> calculateNewBet(2)
    4
    """
    return old * 2

def notAhead(take):
    """Return true if the player has no winnings, false otherwise.

    >>> notAhead(0)
    True
    >>> notAhead(1)
    False
    >>> notAhead(-1)
    True
    """
    return take <= 0

def solvent(take, ceiling):
    """Return false if any loss in the take exceeds the ceiling.

    True if the take is not a loss, or the loss is less than the negative
    of the value of ceiling.
    >>> solvent(1, 100)
    True
    >>> solvent(0, 100)
    True
    >>> solvent(-1, 100)
    True
    >>> solvent(-99, 100)
    True
    >>> solvent(-100, 100)
    False
    """
    return take > -ceiling

def calculateNet(net, take):
    """Return a new net, given the latest take.

    >>> calculateNet(0, 10)
    10
    >>> calculateNet(10, -2)
    8
    """
    return net + take

def calculateInsolvencyCount(insolvencies, take):
    """Return a new insolvency count, given the most recent take.

    If the latest session ended in insolvency (a negative take) then the resulting
    insolvency count is incremented.
    >>> calculateInsolvencyCount(10, 1)
    10
    >>> calculateInsolvencyCount(9, -19)
    10
    >>> calculateInsolvencyCount(12, -32)
    13
    """
    if take < 0:
        return insolvencies + 1
    return insolvencies

def calculateNetWinnings(netWinnings, take):
    """Return an updated net winnings value, given the latest take.

    If the take was a win, it is added the the net. If it was a loss, it is ignored.
    >>> calculateNetWinnings(10, 10)
    20
    >>> calculateNetWinnings(10, -23)
    10
    """
    if take > 0:
        return netWinnings + take
    return netWinnings

def calculateNetLosses(netLosses, take):
    """Return an updated net losses value, given the latest take.

    If the latest take was a loss, it is incorporated to the net, otherwise it is
    ignored.
    >>> calculateNetLosses(0, -10)
    -10
    >>> calculateNetLosses(-10, 3)
    -10
    """
    if take < 0:
        return netLosses + take
    return netLosses

def calculateAverageLoss(netLosses, insolvencies):
    """Determine the average value of each loss.

    Returns the average value of each loss, given a net loss, and a loss
    count. The loss count must be a positive integer.
    >>> calculateAverageLoss(-10, 2)
    -5.0
    """
    return netLosses / insolvencies

def printAverageLoss(netLosses, insolvencies):
    """Print a human-readable statement of the average value of each loss.

    >>> printAverageLoss(0, 0)
    Never went insolvent.
    >>> printAverageLoss(-10, 2)
    Average loss amount: $-5.00
    """
    if insolvencies == 0:
        print('Never went insolvent.')
    else:
        averageLoss = calculateAverageLoss(netLosses, insolvencies)
        print(f'Average loss amount: ${averageLoss:1.2f}')

def calculateAverageWin(netWinnings, wins):
    """Return the average value of each win.

    Requires a net win value, and a win
    count. The win count must be a positive integer.
    >>> calculateAverageWin(10, 2)
    5.0
    """
    return netWinnings / wins

def printAverageWin(netWinnings, insolvencies, tries):
    """Print a human-readable statement of the average value of each win.

    >>> printAverageWin(0, 10, 10)
    Never came out ahead.
    >>> printAverageWin(10, 8, 10)
    Average win amount: $5.00
    """
    wins = tries - insolvencies
    if wins == 0:
        print('Never came out ahead.')
    else:
        averageWin = calculateAverageWin(netWinnings, wins)
        print(f'Average win amount: ${averageWin:1.2f}')

def runSimulation():
    """Run the simulation."""
    print("Enter the player's debt ceiling: $", end='')
    ceiling = int(input())
    print("Enter the number of sessions to play: ", end='')
    tries = int(input())

    # The total take (the amount won or lost) over all sessions so far
    net = 0
    # The number of sessions ending in a loss
    insolvencies = 0
    # The sum of the takes from all winning sessions
    netWinnings = 0
    # The sum of the losses from all losing sessions
    netLosses = 0
    # Execute tries number of sessions
    for x in range(tries):
        # The amount won or lost in this session so far
        take = 0
        # The amount to wager next
        bet = 1
        while notAhead(take) and solvent(take, ceiling):
            result = random.randrange(NUM_SLOTS)
            win = isWin(result)
            take = calculateTake(take, win, bet)
            bet = calculateNewBet(bet)
        net = calculateNet(net, take)
        netWinnings = calculateNetWinnings(netWinnings, take)
        netLosses = calculateNetLosses(netLosses, take)
        insolvencies = calculateInsolvencyCount(insolvencies, take)
        # Poor man's progress indicator
        if x % 20000 == 0:
            print('.', end='')
            sys.stdout.flush()
    print('')
    print(f'Insolvent {insolvencies:n} out of {tries:n} attempts.')
    print(f'Net winnings: ${netWinnings:n} Net losses: ${netLosses:n}')
    printAverageWin(netWinnings, insolvencies, tries)
    printAverageLoss(netLosses, insolvencies)
    print(f'Won ${net:n} overall.')


if __name__ == '__main__':
    runSimulation()
