#!/usr/bin/python3

"""Tests for Roulette.py."""

import unittest
import doctest
import roulette

suite = unittest.TestSuite()
suite.addTest(doctest.DocTestSuite(roulette))
runner = unittest.TextTestRunner()
runner.run(suite)
